const {Builder, By, Key, until} = require('selenium-webdriver');
import { exception } from 'console';
import { WebDriverWrapper } from './web-driver.wrapper';

const time = 5;
let seconds = time * 1000;
let minutes = seconds * 60;

run();

async function run(){
    const browser = await new Builder().forBrowser('chrome').build();
    const driver = new WebDriverWrapper(browser);
    await driver.manage().window().maximize();
    await driver.get("https://campus.defensa.gob.es/login/index.php");

    const username = await driver.querySelector("#username");
    await username.sendKeys("pablomartinezgb7@gmail.com");

    const password = await driver.querySelector("#password");
    await password.sendKeys("panchO4015");

    const loginButton = await driver.querySelector("#loginbtn");
    await loginButton.click();

    await driver.querySelector("#main-slider > div > ul > li.flex-active-slide > img");

    await bucle(driver);
}

async function bucle(driver: WebDriverWrapper){
    
    while(true){

        const currentUrl = await driver.getCurrentUrl();
        if (currentUrl == "https://campus.defensa.gob.es/") {
    
            const misCursos = await driver.querySelector("#main-navigation0");
            await misCursos.click();
    
            const curso = await driver.querySelector("#dropdownmain-navigation0 > li > a");
            await curso.click();

            await driver.querySelector("#instance-4-header");

            const cursoUrl = await driver.getCurrentUrl();
            if(cursoUrl == "https://campus.defensa.gob.es/course/view.php?id=13385"){

                await driver.sleep(minutes);
                await driver.get("https://campus.defensa.gob.es/");

            }
            
        }

    }

}